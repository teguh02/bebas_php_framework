<?php 
    // For database library we use medoo.in
    // You can see the documentation here: https://medoo.in/doc/

    $action = request()::get('action');

    switch ($action) {
        case 'insert':
            // Insert code here

            break;

        case 'update':
            // Update code here
            
            break;

        case 'delete':
            $id = request()::get('id');

            // Delete data
            database()->delete('sample_table', [
                    "AND" => [
                        'id' => $id
                    ]
                ]);
            break;
        
    }

    // Select data
    $data = database()->select('sample_table', '*');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Demo database bebas php framework</title>

    <link rel="stylesheet" href="<?= url('css/app.css') ?>" />
</head>
<body>
    <div class="container py-4">
        <h3>
            Demo Database
        </h3>

        <p>
            In this file, we give you a demo of how to use database in bebas php framework.
        </p>

        <table class="table">
            <thead>
                <tr>
                    <td>
                        #
                    </td>
                    <td>
                        Title
                    </td>
                    <td>
                        Body
                    </td>
                    <td>

                    </td>
                </tr>
            </thead>

            <tbody>
                <?php $i = 1 ?>
                <?php foreach($data as $d) { ?>
                    <tr>
                        <td>
                            <?= $i ?>
                        </td>
                        <td>
                            <?= $d['title'] ?>
                        </td>
                        <td>
                            <?= $d['body'] ?>
                        </td>
                        <td>
                            <a onclick="if(!confirm('Are you sure want to delete?')){return false}" href="<?= url('/index/demo_database?action=delete&id='.$d['id']) ?>" class="btn btn-danger">
                                Delete
                            </a>
                        </td>
                    </tr>

                    <?php $i++ ?>
                <?php } ?>
            </tbody>

        </table>
    </div>
</body>
</html>