<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome To bebas php framework</title>

    <link rel="stylesheet" href="<?= url('css/app.css') ?>" />
</head>
<body class="bg-light">
    <div class="container py-5">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <h3 class="text-center">
                    Welcome To bebas php framework
                </h3>

                <br>

                <p>
                    Pada framework ini kamu bisa membuat aplikasi berbasis website dengan mudah bebas dan tidak terikat aturan MVC dari framework tersebut. Kamu bebas mau menulis seperti apa kode yang kamu inginkan
                </p>

                <hr>

                <p>
                    In this framework, you can create website-based applications easily, freely and not bound by the MVC rules of the framework. You are free to write whatever code you want
                </p>

                <p class="text-center fixed-bottom mb-5">
                    <small>
                        Developed by Teguh Rijanandi
                    </small>
                </p>
            </div>
        </div>
    </div>
</body>
</html>