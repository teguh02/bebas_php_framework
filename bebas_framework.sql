-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 30, 2022 at 07:32 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bebas_framework`
--

-- --------------------------------------------------------

--
-- Table structure for table `sample_table`
--

CREATE TABLE `sample_table` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sample_table`
--

INSERT INTO `sample_table` (`id`, `title`, `body`) VALUES
(1, 'Some title 1', 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Obcaecati omnis fugit odit eius molestiae dolorem? Aliquam vitae aliquid deleniti eius et recusandae pariatur voluptates nihil, voluptate sapiente similique cupiditate provident.'),
(2, 'Some title 2', 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Obcaecati omnis fugit odit eius molestiae dolorem? Aliquam vitae aliquid deleniti eius et recusandae pariatur voluptates nihil, voluptate sapiente similique cupiditate provident.'),
(3, 'Some title 3', 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Obcaecati omnis fugit odit eius molestiae dolorem? Aliquam vitae aliquid deleniti eius et recusandae pariatur voluptates nihil, voluptate sapiente similique cupiditate provident.'),
(4, 'Some title 4', 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Obcaecati omnis fugit odit eius molestiae dolorem? Aliquam vitae aliquid deleniti eius et recusandae pariatur voluptates nihil, voluptate sapiente similique cupiditate provident.'),
(5, 'Some title 5', 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Obcaecati omnis fugit odit eius molestiae dolorem? Aliquam vitae aliquid deleniti eius et recusandae pariatur voluptates nihil, voluptate sapiente similique cupiditate provident.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sample_table`
--
ALTER TABLE `sample_table`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sample_table`
--
ALTER TABLE `sample_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
