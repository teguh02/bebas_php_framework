<?php

namespace system\helper;

class csrf {

    public static function generate_token()
    {
        $token = "bebas_" . md5(uniqid(rand(), true));
        $_SESSION['csrf_token'] = $token;
        return $token;
    }

    public static function get_token_from_session()
    {
        if (isset($_SESSION['csrf_token'])) {
            return $_SESSION['csrf_token'];
        } else {
            return false;
        }
    }
}