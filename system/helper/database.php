<?php

namespace system\helper;

// Using Medoo namespace.
use Medoo\Medoo;

class database {

    public static function connect()
    {
        return new Medoo([
            'type' => DATABASE_TYPE,
            'host' => DATABASE_HOST,
            'database' => DATABASE_NAME,
            'username' => DATABASE_USERNAME,
            'password' => DATABASE_PASSWORD,
        ]);
    }

}