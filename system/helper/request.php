<?php

namespace system\helper;

class request {
    
        public static function get($key)
        {
            if (isset($_GET[$key])) {
                return $_GET[$key];
            } else {
                return false;
            }
        }
    
        public static function post($key)
        {
            if (isset($_POST[$key])) {
                return $_POST[$key];
            } else {
                return false;
            }
        }
    
        public static function request($key)
        {
            if (isset($_REQUEST[$key])) {
                return $_REQUEST[$key];
            } else {
                return false;
            }
        }
    
        public static function server($key)
        {
            if (isset($_SERVER[$key])) {
                return $_SERVER[$key];
            } else {
                return false;
            }
        }
    
        public static function session($key)
        {
            if (isset($_SESSION[$key])) {
                return $_SESSION[$key];
            } else {
                return false;
            }
        }
    
        public static function cookie($key)
        {
            if (isset($_COOKIE[$key])) {
                return $_COOKIE[$key];
            } else {
                return false;
            }
        }
        
        public static function request_url()
        {
            return self::server('REQUEST_URI');
        }

        public static function request_http_method()
        {
            return self::server('REQUEST_METHOD');
        }
}