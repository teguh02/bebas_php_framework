<?php

use system\helper\{database, csrf, request};

if (!function_exists('dd')) {
    function dd()
    {
        foreach (func_get_args() as $x) {
            dump($x);
        }
        die;
    }
}

if (!function_exists('request')) {
    function request()
    {
        return request::class;
    }
}

if (!function_exists('database')) {
    function database()
    {
        return database::connect();
    }
}


if (!function_exists('db')) {
    function db()
    {
        return database();
    }
}

if (!function_exists('url')) {
    function url($url = null)
    {
        if ($url) {
            return rtrim(APP_URL, '/') .'/' . trim($url, '/');
        } else {
            return rtrim(APP_URL, '/');
        }
    }
}

if (!function_exists('csrf')) {
    function csrf()
    {
        echo "<input type='hidden' name='_bebas_token' value='" . csrf::get_token_from_session() . "'>";
    }
}

if (!function_exists('csrf_token')) {
    function csrf_token()
    {
        return csrf::get_token_from_session();
    }
}

// Create your own global helper function here