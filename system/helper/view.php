<?php

namespace system\helper;

class view {

    public static function load_system_view(String $html_name, $extension = 'html')
    {
        include_once SYSTEM_PATH . DIRECTORY_SEPARATOR . 'html'. DIRECTORY_SEPARATOR . $html_name . '.' . $extension;
    }
}