<?php

/**
 * This is a main file to control all framework function
 * by teguhrijanandi
 */

session_name("BEBAS_Framework");
session_start();

use system\helper\{
    request, 
    view,
    csrf
};

class kernel {

    public static function Catch(array $env)
    {
        foreach ($env as $key => $value) {
            define(strtoupper($key), $value);
        }

        return (new self);
    }

    // To Run the application
    public function Run()
    {
        if (APP_DEBUG) {
            error_reporting(E_ALL);
            ini_set('display_errors', '1');
        } else {
            error_reporting(0);
            ini_set('display_errors', 0);
        }

        date_default_timezone_set(APP_TIMEZONE);

        self::register_route();
        self::generate_token();

        return true;
    }

    // To Load all auto route
    private static function register_route()
    {
        $request_url = strtok(request::request_url(), '?');
        $http_method = request::request_http_method();
        $get_csrf_token = csrf::get_token_from_session();
        $csrf_token_from_post_proccess = request::post('_bebas_token');

        if (
            in_array($http_method, ['PUT', 'POST', 'PATCH', "DELETE"])
                and
            $get_csrf_token !== $csrf_token_from_post_proccess
        ) {
            return view::load_system_view('419');
            die();
        }

        if ($request_url == "/" or $request_url == '/index') {
            include_once MODULE_PATH . DIRECTORY_SEPARATOR . 'index'. DIRECTORY_SEPARATOR .'index.php';
        } else {
            $actual_path = MODULE_PATH . DIRECTORY_SEPARATOR . str_replace('/', DIRECTORY_SEPARATOR, trim($request_url, '/')) . '.php';
            
            if (file_exists($actual_path)) {
                include_once $actual_path;
            } else {
                return view::load_system_view('404');
            }
        }
    }

    private static function generate_token()
    {
        return csrf::generate_token();
    }

}