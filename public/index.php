<?php

// Please change this line if this line incorrect
include_once "../vendor/autoload.php";
include_once "../system/kernel.php";

$env['app_name'] = 'Bebas PHP Framework';
$env['app_url'] = "http://localhost:9000";
$env['app_debug'] = true;
$env['app_timezone'] = "Asia/Jakarta";

$env['database_type'] = "mysql";
$env['database_host'] = "localhost";
$env['database_name'] = "bebas_framework";
$env['database_username'] = "root";
$env['database_password'] = "";
$env['database_port'] = 3306;

// Don't change this line if you don't know what you are doing
$env['module_path'] = dirname(__DIR__).'/module';
$env['system_path'] = dirname(__DIR__).'/system';

// Run the application
return (new kernel())::Catch($env) 
    -> Run();